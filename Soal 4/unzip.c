#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void download(){
    char* args[] = { "wget", "-O", "hehe.zip", "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp", NULL };
    if (fork() == 0) execvp("wget", args);
    else wait(NULL);
}

void extract(){
    char* args[] = { "unzip", "hehe.zip", NULL };
    if (fork() == 0) execvp("unzip", args);
    else wait(NULL);
}


int main() {
    
    download();

    extract();

    return 0;
}


