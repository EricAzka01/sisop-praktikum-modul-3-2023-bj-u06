#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHM_SIZE 4096
#define NUM_THREADS 4

struct ThreadData {
    int row;
    int (*matrix)[5];
};

void *calculateFactorial(void *arg) {
    struct ThreadData *data = (struct ThreadData *)arg;
    int row = data->row;
    int (*matrix)[5] = data->matrix;
    for (int j = 0; j < 5; j++) {
        unsigned long long factorial = 1;
        int num = matrix[row][j];

        for (int i = 1; i <= num; i++) {
            factorial *= i;
        }

        printf("%llu ", factorial);
    }

    printf("\n");

    pthread_exit(NULL);
}

int main() {
    //Calculate CPU time
    clock_t start, finish;
    double cpu_time;
    start = clock();
    
    // Key untuk shared memory
    key_t key = ftok("kalian.c", 42);

    // Membuat segment shared memory
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    // Mengaitkan segment shared memory ke memori program
    int (*hasil)[5];  // Pointer ke matriks hasil
    hasil = shmat(shmid, NULL, 0);
    if ((int *)hasil == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    // Menyiapkan data thread
    pthread_t threads[NUM_THREADS];
    struct ThreadData threadData[NUM_THREADS];

    // Membuat thread-thread untuk menghitung faktorial
    for (int i = 0; i < NUM_THREADS; i++) {
        threadData[i].row = i;
        threadData[i].matrix = hasil;

        int rc = pthread_create(&threads[i], NULL, calculateFactorial, (void *)&threadData[i]);
        if (rc) {
            exit(1);
        }
    }

    // Bergabung dengan thread-thread yang telah selesai
    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    // Melepaskan shared memory dari memori program
    shmdt(hasil);
    
    
    finish = clock();

    // Calculate CPU time 
    cpu_time = ((double) (finish - start)) / CLOCKS_PER_SEC;

    // Print CPU time used
    printf("CPU time: %f s\n", cpu_time);
    return 0;
}

