#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define SHM_SIZE 4096

unsigned long long faktorial(unsigned long long n) {
    unsigned long long hasil = 1;
    for (unsigned long long i = 1; i <= n; i++) {
        hasil *= i;
    }
    return hasil;
}

int main() {
    // Calculate CPU time
    clock_t start, finish;
    double cpu_time;
    start = clock();
    
    // Key untuk shared memory
    key_t key = ftok("kalian.c", 42);

    // Membuat segment shared memory
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    // Mengaitkan segment shared memory ke memori program
    int (*hasil)[5];  // Pointer ke matriks hasil
    hasil = shmat(shmid, NULL, 0);
    if ((int *)hasil == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    // Menghitung faktorial dari hasil perkalian
    printf("Faktorial dari hasil perkalian:\n");
    for (int i = 0; i < 4; i++) {
        printf(" [ ");
        for (int j = 0; j < 5; j++) {
            unsigned long long int factorial = 1;
            int num = hasil[i][j];

            for (int k = 1; k <= num; k++) {
                factorial *= k;
            }

            printf("%llu ", factorial);
        }
        printf("] \n");
    }

    // Melepaskan shared memory dari memori program
    shmdt(hasil);
    
    finish = clock();

    // Calculate CPU time 
    cpu_time = ((double) (finish - start)) / CLOCKS_PER_SEC;

    // Print CPU time used
    printf("CPU time: %f s\n", cpu_time);

    return 0;
}


