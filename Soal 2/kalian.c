#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define SHM_SIZE 4096

int main() {
    // Key untuk shared memory
    key_t key = ftok("kalian.c", 42);
    
    // Membuat segment shared memory
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }
    
    // Mengaitkan segment shared memory ke memori program
    int (*hasil)[5];  // Pointer ke matriks hasil
    hasil = shmat(shmid, NULL, 0);
    if ((int*) hasil == (int *) -1) {
        perror("shmat");
        exit(1);
    }

    // Menginisialisasi fungsi random
    srand(time(NULL));

    // Matriks pertama (Ordo 4x2)
    int matriks1[4][2];
    int i, j;
    printf("Matriks pertama:\n");
    for (i = 0; i < 4; i++) {
        printf(" [ ");
        for (j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 5 + 1; // nilai random dari 1-5
            printf("%d ", matriks1[i][j]);
        }
        printf("] \n");
    }

    // Matriks kedua (Ordo 2x5)
    int matriks2[2][5];
    printf("Matriks kedua:\n");
    for (i = 0; i < 2; i++) {
        printf(" [ ");
        for (j = 0; j < 5; j++) {
            matriks2[i][j] = rand() % 4 + 1; // nilai random dari 1-4
            printf("%d ", matriks2[i][j]);
        }
        printf("] \n");
    }

    // Perkalian matriks pertama dan matriks kedua
    int k;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            hasil[i][j] = 0;
            for (k = 0; k < 2; k++) {
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }
    // Output hasil perkalian
    printf("Hasil perkalian:\n");
    for (i = 0; i < 4; i++) {
        printf(" [ ");
    for (j = 0; j < 5; j++) {
        printf("%d ", hasil[i][j]);
    }
    printf("] \n");
    }


    // Melepaskan shared memory dari memori program
    shmdt(hasil);

    return 0;
}

