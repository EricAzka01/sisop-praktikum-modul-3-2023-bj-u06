# sisop-praktikum-modul-3-2023-bj-u06


Group Members:

- Eric Azka Nugroho [5025211064]

- Kirana Alivia Enrico [5025211190]

- Talitha Hayyinas Sahala [5025211263]


# Question 1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

- Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
- Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
- Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
- Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
- Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

Catatan:
Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh: 
Huruf A	: 01000001
Huruf a	: 01100001
Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya
Agar lebih mudah, ubah semua huruf kecil ke huruf kapital

lossless.c

**1A**

(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).
based on this requirement we have to create 2 pipes, these pipes is for pass information from one process to another, specifically a parent process to a child process

```
int pipe_fd[2];
    if (pipe(pipe_fd) == -1) {
        printf("Pipe failed\n");
        exit(1);
    }
```
then we have to create the fork to forking a new process and then handling the parent and child processes differently
```
pid_t pid = fork();

    if (pid < 0) {
        printf("Fork failed\n");
        exit(1);
    } else if (pid > 0) { //parent process
        close(pipe_fd[1]);

```
the function of this following code is for open file.txt
```
const char* input_filename = "file.txt";
```
after that Count the frequency of letter occurrences in that file
```
void count_freq(const char* filename, unsigned int freq[MAX_CHAR]) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Cannot open file: %s\n", filename);
        exit(1);
    }

    int c;
    while ((c = fgetc(file)) != EOF) { //membaca karakter sampai akhir
        freq[c]++; //menghitung frequency tiap karakter
    }

    fclose(file);
}
```
- if **(file == NULL)** the file will cannot to open
- the **while loop** is for loop that reads every character from the file one by one until all characters from the file have been read
- **freq[c]++** This line increments the value in the freq array at the index corresponding to the ASCII value of the character read
- and last after all characters have been read and their frequencies counted, **fclose(file)** will close the file

**1B**
Next, we have to create the Huffman Tree 
first we need the sructure node 
```
struct Node {
    char data;
    unsigned freq;
    struct Node *left, *right;
};
```
then we make a new node for initializes its fields, and returns a pointer to it
```
struct Node* create_node(char data, unsigned freq) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = NULL;
    node->data = data;
    node->freq = freq;
    return node;
}
```
- **struct Node* create_node(char data, unsigned freq)** this function create_node takes two parameters, a character data and an unsigned integer freq
-  **struct Node* node = (struct Node*)malloc(sizeof(struct Node))** this code is allocates memory for a new node of type struct Node using malloc
-  **node->left = node->right = NULL;** initializes the new node since it doesn't have any children ye
- **node->data = data; node->freq = freq;** initialize the data and freq fields of the new node

In the Huffman algorithm, this following function is designed to create a new parent node that merges two existing nodes, typically used in the construction of the Huffman tree.
```
struct Node* mergeNode(struct Node* left, struct Node* right) {
    struct Node* node = create_node('\0', left->freq + right->freq);
    node->left = left;
    node->right = right;
    return node;
}
```
- **struct Node* node = create_node('\0', left->freq + right->freq);** creates a new node from 2 frequencies. The data for this new node is '\0', signifying that it is an internal node (not a leaf node), and the frequency is the sum of the frequencies of the left and right nodes.

after that we create the Huffman Tree
```
struct Node* create_huffman(unsigned int freq[MAX_CHAR]) {
    struct Node *nodes[MAX_CHAR];
    int i, j;
    for (i = 0, j = 0; i < MAX_CHAR; i++) {
        if (freq[i] > 0) {
            nodes[j] = create_node(i, freq[i]);
            j++;
        }
    }

    int size = j;
    while (size > 1) {
        struct Node *left = nodes[size - 2];
        struct Node *right = nodes[size - 1];
        struct Node *merged = mergeNode(left, right);
        nodes[size - 2] = merged;
        size--;
    }

    return nodes[0];
}
```
- The for loop iterates over each character ASCII value. If the frequency of a character is more than 0, a new node is created with the character and its frequency, and the node is added to the nodes array.
- The while loop will create nodes through the merge_node function until the remaining nodes are 1, so the loop will run as long as (size > 1)


this function is used to generate the Huffman codes for each character in the file using the Huffman tree
```
void generate_code(struct Node* root, char* code, int depth, char* huffCode[MAX_CHAR]) {
    if (root == NULL) {
        return;
    }

    if (root->left == NULL && root->right == NULL) {
        code[depth] = '\0';
        huffCode[root->data] = strdup(code);
    }

    code[depth] = '0';
    generate_code(root->left, code, depth + 1, huffCode);

    code[depth] = '1';
    generate_code(root->right, code, depth + 1, huffCode);

    if (depth > 0) {
        free(huffCode[root->data]);
    }
}
```
- firstly this function will check if (root == NULL) will returns without doing anything
- This function will create a Huffman code by giving a code 0 for each left leaf in the tree and will provide a code 1 for each right leaf. During recursion, the code string will be stored incrementally in huffCode[root->data] = strdup(code); until it reaches the character in question
-  the huffCode array will hold the Huffman codes for all the characters in the file. These Huffman codes can then be used to compress the file.

**1C**
this following code is used to save the Huffman tree to a file
```
void save_huffman(struct Node* root, FILE* file) {
    if (root == NULL) {
        return;
    }

    if (root->left == NULL && root->right == NULL) {
        fputc('1', file);
        fputc(root->data, file);
    } else {
        fputc('0', file);
        save_huffman(root->left, file);
        save_huffman(root->right, file);
    }
}
```
then we create code for compress the file
```
void compress_file(const char* input_filename, const char* output_filename, char* huffCode[MAX_CHAR]) {
    FILE* input_file = fopen(input_filename, "r");
    if (input_file == NULL) {
        printf("Cannot open input file: %s\n", input_filename);
        exit(1);
    }
    FILE* output_file = fopen(output_filename, "wb");
    if (output_file == NULL) {
        printf("Cannot create output file: %s\n", output_filename);
        exit(1);
    }
    int c;
    while ((c = fgetc(input_file)) != EOF) {
        char* code = huffCode[c];
        fwrite(code, sizeof(char), strlen(code), output_file);
    }
    fclose(input_file);
    fclose(output_file);
}
```
- a loop that reads characters from the input file one at a time until it reaches the end of the file (EOF)
- then writes the Huffman code to the output file using the fwrite function, each c character in the huffCode string will be stored in the variable code, then the code will be written to the output file with strlen(code).

**1D**
after that we count the number of bits before the file is compressed using this following code
```
unsigned long long int original_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            original_bits += freq[i] 
```
**1E**
then count the number of bits after being compressed with the huffman algorithm and show the total bits of 2 files
```
        unsigned long long int compressed_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            if (freq[i] > 0 && huffCode[i] != NULL) {
                compressed_bits += freq[i] * strlen(huffCode[i]);
            }
        }

        printf("total bits original file: %llu\n", original_bits);
        printf("total bits compressed file: %llu\n", compressed_bits);
```


# Question 2
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.
a. Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.
b. Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)
c. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

maka:

1 2 6 24 120 720 ... ... …
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)
d. Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

**2A**
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define SHM_SIZE 4096

int main() {
    // Key untuk shared memory
    key_t key = ftok("kalian.c", 42);
    
    // Membuat segment shared memory
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }
    
    // Mengaitkan segment shared memory ke memori program
    int (*hasil)[5];  // Pointer ke matriks hasil
    hasil = shmat(shmid, NULL, 0);
    if ((int*) hasil == (int *) -1) {
        perror("shmat");
        exit(1);
    }

    // Menginisialisasi fungsi random
    srand(time(NULL));

    // Matriks pertama (Ordo 4x2)
    int matriks1[4][2];
    int i, j;
    printf("Matriks pertama:\n");
    for (i = 0; i < 4; i++) {
        printf(" [ ");
        for (j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 5 + 1; // nilai random dari 1-5
            printf("%d ", matriks1[i][j]);
        }
        printf("] \n");
    }

    // Matriks kedua (Ordo 2x5)
    int matriks2[2][5];
    printf("Matriks kedua:\n");
    for (i = 0; i < 2; i++) {
        printf(" [ ");
        for (j = 0; j < 5; j++) {
            matriks2[i][j] = rand() % 4 + 1; // nilai random dari 1-4
            printf("%d ", matriks2[i][j]);
        }
        printf("] \n");
    }

    // Perkalian matriks pertama dan matriks kedua
    int k;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            hasil[i][j] = 0;
            for (k = 0; k < 2; k++) {
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }
    // Output hasil perkalian
    printf("Hasil perkalian:\n");
    for (i = 0; i < 4; i++) {
        printf(" [ ");
    for (j = 0; j < 5; j++) {
        printf("%d ", hasil[i][j]);
    }
    printf("] \n");
    }


    // Melepaskan shared memory dari memori program
    shmdt(hasil);

    return 0;
}

**Explanation**
This code performs matrix multiplication. It generates two random matrices, matrix1 and matrix2, multiplies them together, and stores the result in the result matrix. Finally, it prints the original matrices and the resulting matrix.

Here's a step-by-step explanation of the code:

The code includes the necessary header files: <stdio.h>, <stdlib.h>, and <time.h>.

The main() function is the entry point of the program.

Three 2D arrays are declared: matrix1, matrix2, and result. matrix1 is a 4x2 matrix, matrix2 is a 2x5 matrix, and result is a 4x5 matrix.

The srand(time(NULL)) function initializes the random number generator with the current time as the seed value. This ensures that each time the program runs, different random numbers will be generated.

The nested for loops from line 14 to 19 populate matrix1 with random numbers between 1 and 5.

The nested for loops from line 23 to 28 populate matrix2 with random numbers between 1 and 4.

The nested for loops from line 33 to 40 print the elements of matrix1 row by row.

The nested for loops from line 45 to 52 print the elements of matrix2 row by row.

The nested for loops from line 57 to 64 perform the matrix multiplication. Each element of the result matrix is calculated by multiplying the corresponding elements from matrix1 and matrix2 and accumulating the sum in the variable total.

The nested for loops from line 69 to 76 print the elements of the result matrix row by row.

The program ends with return 0;.

Overall, this code generates random matrices, multiplies them, and prints the original matrices as well as the result of the matrix multiplication.

**2B & 2C** 

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHM_SIZE 4096
#define NUM_THREADS 4

struct ThreadData {
    int row;
    int (*matrix)[5];
};

void *calculateFactorial(void *arg) {
    struct ThreadData *data = (struct ThreadData *)arg;
    int row = data->row;
    int (*matrix)[5] = data->matrix;
    for (int j = 0; j < 5; j++) {
        unsigned long long factorial = 1;
        int num = matrix[row][j];

        for (int i = 1; i <= num; i++) {
            factorial *= i;
        }

        printf("%llu ", factorial);
    }

    printf("\n");

    pthread_exit(NULL);
}

int main() {
    //Calculate CPU time
    clock_t start, finish;
    double cpu_time;
    start = clock();
    
    // Key untuk shared memory
    key_t key = ftok("kalian.c", 42);

    // Membuat segment shared memory
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    // Mengaitkan segment shared memory ke memori program
    int (*hasil)[5];  // Pointer ke matriks hasil
    hasil = shmat(shmid, NULL, 0);
    if ((int *)hasil == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    // Menyiapkan data thread
    pthread_t threads[NUM_THREADS];
    struct ThreadData threadData[NUM_THREADS];

    // Membuat thread-thread untuk menghitung faktorial
    for (int i = 0; i < NUM_THREADS; i++) {
        threadData[i].row = i;
        threadData[i].matrix = hasil;

        int rc = pthread_create(&threads[i], NULL, calculateFactorial, (void *)&threadData[i]);
        if (rc) {
            exit(1);
        }
    }

    // Bergabung dengan thread-thread yang telah selesai
    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    // Melepaskan shared memory dari memori program
    shmdt(hasil);
    
    
    finish = clock();

    // Calculate CPU time 
    cpu_time = ((double) (finish - start)) / CLOCKS_PER_SEC;

    // Print CPU time used
    printf("CPU time: %f s\n", cpu_time);
    return 0;
}

**Explanation**
This code performs matrix multiplication and calculates the factorial of each element in parallel using multiple threads. It utilizes shared memory to store the matrix and allows multiple threads to access and compute the factorial concurrently.

Here's a step-by-step explanation of the code:

The code includes the necessary header files: <stdio.h>, <stdlib.h>, <pthread.h>, <sys/types.h>, <sys/ipc.h>, <sys/shm.h>, and <unistd.h>.

The SHM_SIZE constant defines the size of the shared memory segment.

The NUM_THREADS constant defines the number of threads to be created.

The ThreadData struct is defined to hold the data required by each thread. It includes the row number and a pointer to the matrix.

The calculateFactorial function is the entry point for each thread. It takes a void pointer argument and casts it to a ThreadData struct pointer. It then calculates the factorial of each element in the given row of the matrix and prints the result.

In the main function, the clock function is used to measure the CPU time.

The ftok function generates a unique key for the shared memory segment.

The shmget function creates a shared memory segment using the key and the specified size. If it fails, an error message is displayed and the program exits.

The shmat function attaches the shared memory segment to the program's memory. The resulting pointer is assigned to the hasil variable, which is a pointer to a matrix.

An array of pthread_t and ThreadData structs is created to hold the thread IDs and thread data, respectively.

The pthread_create function is called to create multiple threads. Each thread is assigned a row number and a pointer to the shared matrix. The calculateFactorial function is used as the thread routine.

The pthread_join function is called to wait for all threads to finish their execution.

The shmdt function detaches the shared memory segment from the program's memory.

The finish = clock() statement records the finish time.

The CPU time is calculated by subtracting the start time from the finish time and dividing it by CLOCKS_PER_SEC.

The CPU time is printed.

The program ends with return 0;.

Note: The code assumes that the shared memory segment has already been created and filled with the matrix data. The code for populating the shared matrix is not included in this code snippet.

**2D**

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define SHM_SIZE 4096

unsigned long long faktorial(unsigned long long n) {
    unsigned long long hasil = 1;
    for (unsigned long long i = 1; i <= n; i++) {
        hasil *= i;
    }
    return hasil;
}

int main() {
    // Calculate CPU time
    clock_t start, finish;
    double cpu_time;
    start = clock();
    
    // Key untuk shared memory
    key_t key = ftok("kalian.c", 42);

    // Membuat segment shared memory
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    // Mengaitkan segment shared memory ke memori program
    int (*hasil)[5];  // Pointer ke matriks hasil
    hasil = shmat(shmid, NULL, 0);
    if ((int *)hasil == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    // Menghitung faktorial dari hasil perkalian
    printf("Faktorial dari hasil perkalian:\n");
    for (int i = 0; i < 4; i++) {
        printf(" [ ");
        for (int j = 0; j < 5; j++) {
            unsigned long long int factorial = 1;
            int num = hasil[i][j];

            for (int k = 1; k <= num; k++) {
                factorial *= k;
            }

            printf("%llu ", factorial);
        }
        printf("] \n");
    }

    // Melepaskan shared memory dari memori program
    shmdt(hasil);
    
    finish = clock();

    // Calculate CPU time 
    cpu_time = ((double) (finish - start)) / CLOCKS_PER_SEC;

    // Print CPU time used
    printf("CPU time: %f s\n", cpu_time);

    return 0;
}

**Explanation**
This code calculates the factorial of each element in the shared matrix that was created by a different program. It retrieves the shared memory segment, reads the matrix data, and computes the factorial for each element.

Here's a step-by-step explanation of the code:

The code includes the necessary header files: <stdio.h>, <stdlib.h>, <sys/types.h>, <sys/ipc.h>, <sys/shm.h>, <unistd.h>, and <time.h>.

The SHM_SIZE constant defines the size of the shared memory segment.

The faktorial function takes an unsigned long long integer n as input and calculates its factorial using a simple iterative loop.

In the main function, the clock function is used to measure the CPU time.

The ftok function generates a unique key for the shared memory segment.

The shmget function retrieves the shared memory segment using the key and the specified size. If it fails, an error message is displayed and the program exits.

The shmat function attaches the shared memory segment to the program's memory. The resulting pointer is assigned to the hasil variable, which is a pointer to a matrix.

The code then loops through each element in the matrix and calculates its factorial using the faktorial function. The resulting factorial is printed.

The shmdt function detaches the shared memory segment from the program's memory.

The finish = clock() statement records the finish time.

The CPU time is calculated by subtracting the start time from the finish time and dividing it by CLOCKS_PER_SEC.

The CPU time is printed.

The program ends with return 0;.

This code assumes that the shared memory segment was created and filled with the matrix data by a different program. It only reads the data from the shared memory and calculates the factorial for each element. Make sure to run the appropriate program that creates the shared memory and fills it with the matrix data before executing this code.


# Question 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.
Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.
User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket sehingga struktur direktorinya adalah sebagai berikut:
└── soal3
	├── playlist.txt
	├── song-playlist.json
	├── stream.c
	└── user.c


Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt
Sample Output:
17 - MK
1-800-273-8255 - Logic
1950 - King Princess
…
Your Love Is My Drug - Kesha
YOUTH - Troye Sivan
ZEZE (feat. Travis Scott & Offset) - Kodak Black


User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan sebagai berikut.
PLAY "Stereo Heart" 
    sistem akan menampilkan: 
    USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART"
PLAY "BREAK"
    sistem akan menampilkan:
    THERE ARE "N" SONG CONTAINING "BREAK":
    1. THE SCRIPT - BREAKEVEN
    2. ARIANA GRANDE - BREAK FREE
dengan “N” merupakan banyaknya lagu yang sesuai dengan string query. Untuk contoh di atas berarti THERE ARE "2" SONG CONTAINING "BREAK":
PLAY "UVUWEVWEVWVE"
    THERE IS NO SONG CONTAINING "UVUVWEVWEVWE"

Untuk mempermudah dan memperpendek kodingan, query bersifat tidak case sensitive 😀
User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut:
User mengirimkan perintah
ADD <SONG1>
ADD <SONG2>
sistem akan menampilkan:
USER <ID_USER> ADD <SONG1>

User dapat mengedit playlist secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”
Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.
Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

Catatan: 
Untuk mengerjakan soal ini dapat menggunakan contoh implementasi message queue pada modul.
Perintah DECRYPT akan melakukan decrypt/decode/konversi dengan metode sebagai berikut:
ROT13
ROT13 atau rotate 13 merupakan metode enkripsi sederhana untuk melakukan enkripsi pada tiap karakter di string dengan menggesernya sebanyak 13 karakter.
Contoh:

Base64
Base64 adalah sistem encoding dari data biner menjadi teks yang menjamin tidak terjadinya modifikasi yang akan merubah datanya selama proses transportasi. Tools Decode/Encode.
Hex
Hexadecimal string merupakan kombinasi bilangan hexadesimal (0-9 dan A-F) yang merepresentasikan suatu string. Tools.

Implementing a message queue.

Users can send command strings to the system, and the results of the commands are sent to stream.c through a message queue.
--user.c
```
//struct untuk message buffer
struct msg_buf{

    int id;
    long msg_type;
    char msg_text[500];
};

int main(){

    struct msg_buf data;
    int msg_id;

    //melakukan inisialisasi bahwa id message queue yaitu 12
    key_t key;
    key = 12;

    //membuat msg_id
    msg_id = msgget(key, 0666|IPC_CREAT);

    if(msg_id == -1){
        puts("Error in creating queue\n");
        exit(0);
    }  
    ...
```
Here, we declare the presence of a message queue with ID 12. Then, IPC_CREAT is used to create the message queue if it doesn't already exist. The meaning of 0666 is that it allows both writing and reading to occur.
stream.c
```
int running = 1;
    char buffer_msg[200];

    while(running){

        puts("Masukkan perintah yang sesuai dengan format: ");
        puts("* Untuk menambahkan lagu, ketik \"ADD Judul_lagu\"");
        puts("* Untuk play lagu, ketik \"PLAY \"Judul_Lagu\"\"");
        puts("* Untuk decrypt maka ketik \"DECRYPT\"");
        puts("* Untuk memunculkan daftar lagu, ketik \"LIST\"");
        puts("* Untuk selesai, maka ketik \"end\"");

        //memasukkan inputan ke buffer_msg
        fgets(buffer_msg, 200, stdin);


        //cek apakah stdin berupa perintah DECRYPT, LIST, PLAY, ataupun ADD
        if(strstr(buffer_msg, "DECRYPT") || strstr(buffer_msg, "LIST") || strstr(buffer_msg, "PLAY") || strstr(buffer_msg, "ADD")){
            data.msg_type = 1;
            
            //mendapatkan nilai getpid() untuk membedakan user 1 dengan user lainnya yang sedang akses file user.c. Karena, kita hanya membatasi hingga 2 user saja. 
            data.id = getpid();

            //copy buffer_msg ke struct data.msg_txt
            strcpy(data.msg_text, buffer_msg);
            
            //lalu struct data dikirim ke message queue
            if(msgsnd(msg_id, (void*) &data, 500, 0) == -1) puts("Message not sent");
        }

        else if(strstr(buffer_msg, "end")){

            //mendapatkan pid agar kita tau mana process id yang telah berhenti
            data.id = getpid();
            data.msg_type = 1;

            strcpy(data.msg_text, "end");
            msgsnd(msg_id, (void*) &data, 500, 0);

            exit(EXIT_FAILURE);
        }

        else{
            data.id = getpid();
            data.msg_type = 1;
            
            //apabila commandnya tidak sesuai maka outputnya harus "UNKNOWN COMMAND"
            strcpy(data.msg_text, "UNKNOWN COMMAND");
            msgsnd(msg_id, (void*) &data, 500, 0);
        }

    }
```
Here, a continuous looping process will be performed, but we limit it to only 2 users who can send commands and have them read by stream.c. Therefore, to differentiate one process from another, we use getpid().

For the DECRYPT command, we need to modify 3 methods: ROT13, HEX, and Base64. Here, I have taken a template from Google.

For ROT13:
```
char *rot13(char *src){

    if(src == NULL) return NULL;

    char* result = malloc(strlen(src) + 1);

    if(result != NULL){
        strcpy(result, src);

        char* current_char = result;

        while(*current_char != '\0'){

            if((*current_char >= 97 && *current_char <= 122) || (*current_char >= 65 && *current_char <= 90)){

                if(*current_char > 109 || (*current_char > 77 && *current_char < 91)){

                    *current_char -= 13;
                }
                else{
                    *current_char += 13;
                }
            }
            current_char++;
        }
    }

    return result;
}
```
for HEX:
```
char* hexToASCII(const char* hex) {
    size_t len = strlen(hex);
    if (len % 2 != 0) return NULL; // Return NULL jika jumlah digit hex bukan genap

    size_t output_len = len / 2;
    char* output = malloc(output_len + 1);
    if (output == NULL) return NULL; // Return NULL jika alokasi memori gagal

    for (size_t i = 0; i < output_len; i++) {
        char byte[3] = {hex[i * 2], hex[i * 2 + 1], '\0'}; // Ambil 2 digit hex untuk setiap byte ASCII
        output[i] = (char)strtol(byte, NULL, 16); // Convert hex menjadi bilangan desimal
    }
    output[output_len] = '\0'; // Tambahkan null terminator pada akhir string ASCII

    return output;
}
```
for Base64
```
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
 
void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

unsigned char *base64(const char *data,
                             size_t input_length,
                             size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    unsigned char *hasil_decode = malloc(*output_length);

    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    for (int i = 0; i < *output_length; i++){

        if(decoded_data[i] != 0 && decoded_data[i] != 127){
            hasil_decode[i] = decoded_data[i];
        }
        else break;
    }
 
    return hasil_decode;
}
```
then, we handle the results from the message queue with an ID of 12. Before that, because we restrict access to only 2 users, we create a struct to handle the process ID (PID)
```
//struct untuk pid_shared_t
typedef struct {
    pid_t pid1;
    pid_t pid2;
} pid_shared_t;
```
Next, we perform semaphore initialization
```
// Inisialisasi semaphore
sem_t semaphore;
if (sem_init(&semaphore, 1, 1) == -1) {
    perror("sem_init");
    exit(EXIT_FAILURE);
}

```
We also do the initialization of shared memory
```
// Inisialisasi shared memory
int shem_id = shmget(SHM_KEY, sizeof(pid_shared_t), IPC_CREAT | 0666);
if (shem_id == -1) {
    perror("shmget");
    exit(EXIT_FAILURE);
}

pid_shared_t *pid_data = shmat(shem_id, NULL, 0);
if (pid_data == (void *) -1) {
    perror("shmat");
    exit(EXIT_FAILURE);
}

```
Then, we do data retrieval for only 2 user handles.
```
    while(running){

        // sem_destroy(&semaphore);
        // shmdt(pid_data);
        // shmctl(shem_id, IPC_RMID, NULL);

        msgrcv(msg_id, (void*) &data, BUFSIZ, msg_rcv, 0);

        // Akses shared memory dan semaphore
        sem_wait(&semaphore); // Mengurangi nilai semaphore menjadi 0

        // Menghandle kondisi jika pid1 masih bernilai 0 dan nilai pid dari messagequeue tidak sama dengan pid2. Atau kondisi dimana pid dari messagequeue sama dengan pid1. 
        if ((pid_data->pid1 == 0 && data.id != pid_data->pid2) || data.id == pid_data->pid1) {
            pid_data->pid1 = data.id;
            
            //bila printah merupakan "end", maka user tersebut tidak dapat mengirim perintah lagi
            if(strncmp(data.msg_text, "end", 3) == 0){
                printf("Process with PID %d is end.\n", data.id);

                printf("Data received: %s\n", data.msg_text);

                pid_data->pid1 = 0;

                flag = 0;

                sem_post(&semaphore); // Meningkatkan nilai semaphore kembali menjadi 1

            }
            
            else{
                printf("Process with PID %d is running.\n", data.id);
                flag = 1;
                sem_post(&semaphore);
            } 
        } 

        // Menghandle kondisi jika pid2 masih bernilai 0 dan nilai pid dari messagequeue tidak sama dengan pid1. Atau kondisi dimana pid dari messagequeue sama dengan pid2. 
        else if ((pid_data->pid2 == 0 && data.id != pid_data->pid1) || data.id == pid_data->pid2) {
            pid_data->pid2 = data.id;
            
            //bila printah merupakan "end", maka user tersebut tidak dapat mengirim perintah lagi
            if(strncmp(data.msg_text, "end", 3) == 0){

                printf("Process with PID %d is end.\n", data.id);
                printf("Data received: %s\n", data.msg_text);

                pid_data->pid2 = 0;

                flag = 0;

                sem_post(&semaphore); // Meningkatkan nilai semaphore kembali menjadi 1

            }

            else {
                printf("Process with PID %d is running.\n", data.id);
                flag = 1;
                sem_post(&semaphore);
            }
        } 
        
        //handle apabila pid1 dan pid2 telah penuh maka terjadi overload. 
        else{

            printf("Process with PID %d is unacceptable.\n", data.id);
            puts("STREAM SYSTEM OVERLOAD");
            flag = 0;
            sem_post(&semaphore);
        }

        sem_wait(&semaphore);
        ...
```
If flag = 1 then the process of calling the command occurs (not from the end command)
```
if(flag == 1){
    
    printf("Data received: %s with id: %d\n", data.msg_text, data.id);

    //jika data message = DECRYPT, maka memanggil fungsi DECRYPT()
    if(strncmp(data.msg_text, "DECRYPT", 7) == 0){

        DECRYPT();
        puts("");
    }

    //jika data message = LIST, maka memanggil fungsi LIST()
    else if(strncmp(data.msg_text, "LIST", 4) == 0){

        LIST();
        puts("");
    }

    //jika data message = PLAY, maka memanggil fungsi PLAY()
    else if(strncmp(data.msg_text, "PLAY", 4) == 0){

        //karena inputan user dari perintah play adalah 
        //contoh: PLAY "Flo"
        // Kita ingin mengambil kalimat Flo saja. 

        //Mencari karakter pertama dari tanda kutip
        char *ptr1 = strchr(data.msg_text, '\"');
        char *ptr2;

        char lagu_input[200] = {0};
        int user_id = -1;

        if(ptr1 != NULL){

            //pointer menunjuk ke karakter setelah tanda kutip
            ptr1++;

            //Mencari karakter kedua dari tanda kutip 
            ptr2 = strchr(ptr1, '\"');

            //Copy substring antara ptr1 dengan ptr2 ke array lagu_input dengan panjang substring dihitung dengan mengurangi ptr1 dari ptr2
            if(ptr2 != NULL) strncpy(lagu_input, ptr1, ptr2 - ptr1);
        }

        PLAY(data.id, lagu_input);
    }
    
    //jika data message = ADD, maka memanggil fungsi ADD()
    else if(strncmp(data.msg_text, "ADD", 3) == 0){

                //karena inputan user dari perintah ADD adalah 
        //contoh: ADD Flower - Jisoo
        // Kita ingin mengambil kalimat Flower - Jisoo saja. 

        //mencari substring dari ADD 
        char *ptr1 = strstr(data.msg_text, "ADD ");
        if (ptr1 != NULL) {
            ptr1 += 4; // maju sebanyak 4 karakter untuk melewati "CEK "
            char *ptr2 = strchr(ptr1, '\0'); // cari akhir string

            // menghapus whitespace di awal string
            while (*ptr1 == ' ') ptr1++;
            
            // menghapus whitespace di akhir string
            while (*(ptr2 - 1) == ' ') ptr2--;

            // menghitung panjang kalimat yang diambil
            size_t len = ptr2 - ptr1;

            // menyalin kalimat ke buffer
            char lagu_input[200];
            strncpy(lagu_input, ptr1, len);
            lagu_input[len] = '\0';

            ADD(data.id, lagu_input);
        }
    }
}
```
If you want to call the DECRYPT function then:
```
void DECRYPT(){

    char* json_data;
    char *filename = "song-playlist.json";
    FILE *file = fopen(filename, "r");

    if(file == NULL){
        puts("Gagal membuka file");
        exit(1);
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    rewind(file);

    json_data = (char*) malloc(file_size);

    fread(json_data, file_size, 1, file);

    fclose(file);

    char *json_start = strchr(json_data, '[');
    char *json_end = strchr(json_data, ']');

    char *method_start = strstr(json_start, "\"method\"");

    FILE *fp = fopen("playlist.txt", "w");

    while(method_start && method_start < json_end) {

        // Mengambil nilai method
        char *method_value_start = strchr(method_start, ':');
        char *method_value_end = strchr(method_value_start, ',');
        int method_value_len = method_value_end - method_value_start - 3;

        char method[method_value_len + 1];
        strncpy(method, method_value_start + 3, method_value_len);
        method[method_value_len] = '\0';

        strtok(method, "\"");

        printf("Method: %s\n", method);

        // Mengambil nilai song
        char *song_start = strstr(method_value_end, "\"song\"");
        char *song_value_start = strchr(song_start, ':');
        char *song_value_end = strchr(song_value_start, '}');
        long song_value_len = song_value_end - song_value_start - 3;

        char song[song_value_len + 1];
        strncpy(song, song_value_start + 3, song_value_len);
        song[song_value_len] = '\0';

        strtok(song, "\"");

        printf("Song: %s\n", song);
        
        //mulai decrypt:
        if(strcmp(method, "rot13") == 0){
            puts("INI ROT13");

            char *result = rot13(song);
            printf("Hasil decrypt dari rot13: %s\n",result);

            fprintf(fp, "%s\n", result);
        } 

        if(strcmp(method, "base64") == 0){

            char* hasil_decode = base64(song, song_value_len, &song_value_len);

            printf("Hasil decrypt dari base64: %s\n", hasil_decode);

            fprintf(fp, "%s\n", hasil_decode);
        } 

        if(strcmp(method, "hex") == 0){

            char* hexString = hexToASCII(song);

            printf("Hasil hex to string: %s\n", hexString);

            fprintf(fp, "%s\n", hexString);

        } 

        // Pindah ke objek method dan song berikutnya
        method_start = strstr(song_value_end, "\"method\"");
    }

    fclose(fp);
}

```
If you want to call the LIST function, then:
```
// Fungsi untuk membandingkan dua string
int compare(const void* a, const void* b) {
    char* str1 = *(char**)a;
    char* str2 = *(char**)b;

    return strcmp(str1, str2);
}

void LIST() {

    FILE *file_song = fopen("playlist.txt", "r");
    if (file_song == NULL) {
        puts("File tidak ditemukan!");
    }

    char line[105];
    char lines[1005][105];
    char* uppercaseLines[1005];
    char* lowercaseLines[1005];
    char* numberLines[1005];
    int numUppercaseLines = 0;
    int numLowercaseLines = 0;
    int numNumberLines = 0;

    // Baca setiap baris dan cek huruf pertamanya
    for(int i = 0; i < 1005 && fgets(line, 105, file_song); i++) {
        
        //cek huruf besar
        if (line[0] >= 65 && line[0] <= 90) {
            // Alokasikan memori dan salin isi baris ke array untuk huruf besar
            uppercaseLines[numUppercaseLines] = lines[i];
            strcpy(uppercaseLines[numUppercaseLines], line);
            numUppercaseLines++;
        } 
        //cek huruf kecil
        else if (line[0] >= 97 && line[0] <= 122) {
            // Alokasikan memori dan salin isi baris ke array untuk huruf kecil
            lowercaseLines[numLowercaseLines] = lines[i];
            strcpy(lowercaseLines[numLowercaseLines], line);
            numLowercaseLines++;
        } 
        //cek angka dsb
        else{
            // Alokasikan memori dan salin isi baris ke array untuk angka
            numberLines[numNumberLines] = lines[i];
            strcpy(numberLines[numNumberLines], line);
            numNumberLines++;
        }
    }

    FILE *sort_file = fopen("sortingSong.txt", "w");
    
    //sorting 
    qsort(uppercaseLines, numUppercaseLines, sizeof(char*), compare);
    qsort(lowercaseLines, numLowercaseLines, sizeof(char*), compare);
    qsort(numberLines, numNumberLines, sizeof(char*), compare);

    //print untuk urutan angka dulu
    for(int i = 0; i < numNumberLines; i++){
        fputs(numberLines[i], sort_file);
        printf("%s\n", numberLines[i]);
    }
    //print untuk huruf kecil
    for(int i = 0; i < numLowercaseLines; i++){
        fputs(lowercaseLines[i], sort_file);
        printf("%s\n", lowercaseLines[i]);
    }
    //print untuk huruf besar
    for(int i = 0; i < numUppercaseLines; i++){
        fputs(uppercaseLines[i], sort_file);
        printf("%s\n", uppercaseLines[i]);
    }

    fclose(sort_file);

}
```
If you want to call the PLAY() function, then:
```
void PLAY(const int x, const char* words){

    FILE * cek_song;
    char line_cek[150];
    char matchedLines[100][150];
    long numMatched = 0;

    cek_song = fopen("playlist.txt", "r");

    //ambil lagu di playlist.txt
    while(fgets(line_cek, 150, cek_song)){

        //diubah ke huruf kecil agar gampang
        char* tmp = strdup(line_cek);

        for(int i = 0; i < strlen(tmp); i++){
            tmp[i] = tolower(tmp[i]);
        }

        //diubah ke huruf kecil untuk kalimat dari user
        char* wordLower = strdup(words);

        for(int i = 0; i < strlen(wordLower); i++){
            wordLower[i] = tolower(wordLower[i]);
        }

        //melakukan cek dengan numMatched
        if(strstr(tmp, wordLower)){
            
            strcpy(matchedLines[numMatched], line_cek);
            numMatched++;
        }
    }

    fclose(cek_song);

    //Maka hanya terdapat 1 lagu, sehingga: 
    if(numMatched == 1) printf("USER %d PLAYING \"%s\"\n", x, matchedLines[0]);
    //Terdapat lebih dari 1 lagu yang memiliki kata yang mirip, sehingga:
    else if(numMatched > 1){
        printf("THERE ARE %ld SONG CONTAINING \"%s\":", numMatched, words);
        puts("");
        
        for(long i = 0; i < numMatched; i++){
            printf("%ld. %s", i + 1, matchedLines[i]);
        }
    }

    //apabila tidak ada lagunya, maka:
    else printf("THERE IS NO SONG CONTAINING \"%s\"\n", words);

}
```
If you want to call the ADD() function, then:
```
void ADD(const int x, const char* newSong){

    FILE *add_song;
    add_song = fopen("playlist.txt", "r");

    char line[150];
    int flag = 0;
    while(fgets(line, 150, add_song)){
        
        //cek apakah ada kalimat yang mirip
        if(strstr(line, newSong)){
            flag = 1;
            break;
        }
    }

    fclose(add_song);

    if(flag == 0){
        add_song = fopen("playlist.txt", "a");

        fputs(newSong, add_song);

        fclose(add_song);

        printf("USER %d ADD %s\n", x, newSong);
    }
    // lagu sudah ada di playlist
    else puts("SONG ALREADY ON PLAYLIST");
}
```
After that:
```
//menghancurkan semaphore
sem_destroy(&semaphore);
//melepas segmen memori 
shmdt(pid_data);

//menghapus segment memori yang dibagikan 
shmctl(shem_id, IPC_RMID, NULL);

//menghapus messagequeue 
msgctl(msg_id, IPC_RMID, 0);

```



# Question 4


Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 
Download dan unzip file tersebut dalam kode c bernama unzip.c.
Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.
Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file
Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
Path dimulai dari folder files atau categorized
Simpan di dalam log.txt
ACCESSED merupakan folder files beserta dalamnya
Urutan log tidak harus sama
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
Untuk menghitung banyaknya ACCESSED yang dilakukan.
Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.


**4A**

In this problem, we were tasked to download and unzip the file that were given in the problem. The code can be seen below,


```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void download(){
    char* args[] = { "wget", "-O", "hehe.zip", "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp", NULL };
    if (fork() == 0) execvp("wget", args);
    else wait(NULL);
}

void extract(){
    char* args[] = { "unzip", "hehe.zip", NULL };
    if (fork() == 0) execvp("unzip", args);
    else wait(NULL);
}


int main() {
    
    download();

    extract();

    return 0;
}


```


**Explaination**

We created two void for this problem, the first one is to download the file that were given in the question. We convert the link into google drive link so that it would be easier to be solved. After converting into the google drive link, we use execvp and "wget" to download the file. 

```
void download(){
    char* args[] = { "wget", "-O", "hehe.zip", "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp", NULL };
    if (fork() == 0) execvp("wget", args);
    else wait(NULL);
}


```

The second void is created to extract or unzip the file. We also use the same method which is execvp. We instruct the execvp to unzip the files of hehe.zip. Then, after both void was created, we simply call them in the main function so that the code can run. 


```
void extract(){
    char* args[] = { "unzip", "hehe.zip", NULL };
    if (fork() == 0) execvp("unzip", args);
    else wait(NULL);
}


```



**4B**


For the second question, our strategy was to create the directory of categorized, create struct for the argument that was needed, then we read the file of "max.txt" and save the result, create function for writing the log, create sub directory according to the extention, combine the thread that are processing together, create "other" subcategory in the directory of "categorized", fill in the subdirectory, and create an output for the file. 

**Creating the directory of categorized**

```
// membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }

```

The code above the check whether directory of categorized has been made or not. If not, then it will create by using mkdir(). Then, it will create function of log ```writeLogMade()```. 


**Creating the struct for the argument that was needed**

 ```
struct ThreadArgs {
    char dirname[MAX_LEN];
    int maxFile;
};

 ```

 The code above is a struct that is named ThreadArgs that has two variable, which are dirname and maxfile. Dirname is an array of character which has the capacity of max_len. While maxfile is a round number to show the number of file. 


**Read the file of "max.txt" and save the result**


```
char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    // membuka file untuk dibaca
    max = fopen(maxfilename, "r");

    // memeriksa apakah file berhasil dibuka
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

     // menutup file
    fclose(max);

```

The variable of maxfilename is define as string that is called "max.txt". Then, we open the file by using fopen. After the file has been opened, we scan the file using fscanf. We will record the maxfil which is used to count the number of directory. Lastly, fclose() is used as a error handling. 


**Create function for writing the log**

```
char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;

    // membuka file untuk dibaca
    fp = fopen(filename, "r");

    // memeriksa apakah file berhasil dibuka
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }


```

The code above will create a function for writing the log. ```Pthread_t tid[MAX_LEN]``` is used as an array of thread that will be used to create the directory. ```struct ThreadArgs thread_args[MAX_LEN]``` is array of struct that is filled with argument of each thread.


**Create sub directory according to the extention**

```
 // membuka file log
    log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        exit(1);
    }

    // inisialisasi mutex lock
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Failed to initialize mutex lock");
        exit(1);
    }

```


The code is an initialization to open the log file and initialize mutex lock in the program. Fopen() is used to open the file of log.txt with "a". It is used to add the new file.


**Log Made**

```
void writeLogMade(char createdDir[]){
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logMADE[256] = "MADE ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMADE, createdDir);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logMADE);
    pthread_mutex_unlock(&lock);
}

```

This function is used to write message of log into the file of log.txt into the directory that has been made by the program. 


**Log Accessed**

```
void writeLogAccess(char folderPath[]){
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logAccess[256] = "ACCESSED ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logAccess, folderPath);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logAccess);
    pthread_mutex_unlock(&lock);
}

```

The above code is define by the function of writeLogAccess which functions to write message to log into the file of log. The function will receieve one argument which is folderpath, that is used to write the mssage into a certain folder. 


**Log Accessed into categorized/directory**

```

sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

```

**Log Accessed for categorized/other**

```
 char logAccessSource[200];
 char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);

    system(logAccessSource);
    system(logAccessTarget);

```


**Log Moved**

```
 sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

```

The code will move file into directory categorized/[file extention]


**Create sub directory according to the extention**


```

// membaca nama direktori dari file, satu per satu
    while (fgets(dirname, MAX_LEN, fp)) {
        // menghilangkan karakter newline di akhir string
        dirname[strcspn(dirname, "\n")] = 0;

        // menghapus spasi di akhir string
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1])) {
            len--;
        }
        dirname[len] = '\0';

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFile = maxFile;
        pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
        i++;
    }


```

The above code will read directory from the file "extention.txt" and will create new directory using thread.


**Counting the Directory**

```
//menghitung total direktori yang dibuat per ext 
    int numDir;
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }
// Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);
```

The above code will count the number of directory based on the number of files in the extention.


**Full Code**

```
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>
#include <math.h> 

#define MAX_LEN 256
#define MAX_PATH 256
#define MAX_EXT 5
#define MAX_DEST_SIZE 475 // maximum size of destination buffer

pthread_mutex_t lock;
FILE* log_file;

struct ThreadArgs {
    char dirname[MAX_LEN];
    int maxFile;
};

void writeLogMade(char createdDir[]){
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logMADE[256] = "MADE ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMADE, createdDir);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logMADE);
    pthread_mutex_unlock(&lock);
}

void writeLogAccess(char folderPath[]){
    // menulis pesan log
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logAccess[256] = "ACCESSED ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logAccess, folderPath);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logAccess);
    pthread_mutex_unlock(&lock);
}


void* create_directory(void* arg) {
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg;
    char* dirname = thread_args->dirname;
    int maxFile = thread_args->maxFile;
    char ext[4];
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/";
    char new_dirname[MAX_LEN];
    struct stat st;

    // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    //hitung banyak file
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE* fp;
    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // ascending output
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
    
    //menghitung total direktori yang dibuat per ext 
    int numDir;
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }

    // Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);

    //create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // directory creation was successful, so do something
        writeLogMade(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
         // mendefinisikan variable nama directory dengan suffix sesuai dengan index
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                // membuat direktori jika belum ada
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //move files according to their extensions
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char command4[600];
                    char logAccessSource[200];
                    char logAccessTarget[100];

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                    if (maxFile == 0)
                    {
                        sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                        system(command4);
                    } else {
                        sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                        system(command1);

                    }


                } else {
                    perror("Gagal membuat direktori");
                    
                }
            }
    }
    return NULL;
}


int main() {
    // membuka file log.txt
    log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        exit(1);
    }

    // inisialisasi mutex lock
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Failed to initialize mutex lock");
        exit(1);
    }

    char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;
    
    // membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }

    //membaca file max.txt dan menyimpan nilainya
    char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    // membuka file untuk dibaca
    max = fopen(maxfilename, "r");

    // memeriksa apakah file berhasil dibuka
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // read the first line of max.txt into firstLine

     // menutup file max.txt
    fclose(max);

    // membuka file extensions.txt untuk dibaca
    fp = fopen(filename, "r");

    // memeriksa apakah file extensions.txt berhasil dibuka
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    // membaca nama direktori dari file, satu per satu
    while (fgets(dirname, MAX_LEN, fp)) {
        // menghilangkan karakter newline di akhir string
        dirname[strcspn(dirname, "\n")] = 0;

        // menghapus spasi di akhir string
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1])) {
            len--;
        }
        dirname[len] = '\0';

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFile = maxFile;
        pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
        i++;
    }

    // join thread yang masih berjalan
    for (int j = 0; j < i; j++) {
        pthread_join(tid[j], NULL);
    }

    // membuat direktori "other" di dalam "categorized" jika belum ada
    if (stat("categorized/other", &st) != 0) {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }

    // move other files to categorized/other
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);
    
    system(logAccessSource);
    system(logAccessTarget);

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);

    //output sort buffer.txt untuk jumlah file tiap extension
    system("sort buffer.txt");
    system("rm -rf buffer.txt");

    // count how many files in dir categorized/other
    DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir (otherPath)) != NULL) {
        writeLogAccess(otherPath);
        while ((ent = readdir (dir)) != NULL) {
            if(ent->d_type == DT_REG) {
                count++;
            }
        }
        closedir (dir);
    } else {
        perror ("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);

    // menutup file
    fclose(fp);
    
   // menghancurkan mutex lock
    pthread_mutex_destroy(&lock);

    // menutup file log
    fclose(log_file);

    return 0;
}


```


**Create Logchecker.c**

For this problem, we need to count the access, create list of folder and total file in categorized/, and lastly create total file for every extention by ascending. 


**Count the access**

```
// Accessed count
    printf ("\e[33mTotal banyak akses:\e[0m\n");
    system("grep ACCESSED log.txt | wc -l");

```

The code above will count total line in the file of log.txt that is using "ACCESSED".

**Create list of folder and total file in categorized/**

```
// List folders & total file
    printf ("\e[33m\nList folder & total file tiap folder:\e[0m\n");
    system("grep 'MOVED\\|MADE' log.txt \
    | awk -F 'categorized' '{print \"categorized\"$NF}' \
    | sed '/^$/d' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");


```

The code will use to show the list of folder and the total file in each folder after the file has been moved.


**Create total File for every extention**

```
// total file tiap extension terurut secara ascending
    printf ("\n\e[33mTotal file tiap extension\e[0m\n");
    system("grep 'MADE\\|MOVED' log.txt \
    | grep -o 'categorized/[^o]*' \
    | sed 's/categorized\\///' \
    | sed '/^$/d' \
    | sed 's/ \\([^o]*\\)//' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");

```

The code above will create total file for every extention (ascending).
