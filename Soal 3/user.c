#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

//struct untuk message buffer
struct msg_buf{

    int id;
    long msg_type;
    char msg_text[500];
};

int main(){

    struct msg_buf data;
    int msg_id;

    //melakukan inisialisasi bahwa id message queue yaitu 12
    key_t key;
    key = 12;

    //membuat msg_id
    msg_id = msgget(key, 0666|IPC_CREAT);

    if(msg_id == -1){
        puts("Error in creating queue\n");
        exit(0);
    }  

    //handle jika inputan user belum selesai
    int running = 1;
    char buffer_msg[200];

    while(running){

        puts("Masukkan perintah yang sesuai dengan format: ");
        puts("* Untuk menambahkan lagu, ketik \"ADD Judul_lagu\"");
        puts("* Untuk play lagu, ketik \"PLAY \"Judul_Lagu\"\"");
        puts("* Untuk decrypt maka ketik \"DECRYPT\"");
        puts("* Untuk memunculkan daftar lagu, ketik \"LIST\"");
        puts("* Untuk selesai, maka ketik \"end\"");

        //memasukkan inputan ke buffer_msg
        fgets(buffer_msg, 200, stdin);


        //cek apakah stdin berupa perintah DECRYPT, LIST, PLAY, ataupun ADD
        if(strstr(buffer_msg, "DECRYPT") || strstr(buffer_msg, "LIST") || strstr(buffer_msg, "PLAY") || strstr(buffer_msg, "ADD")){
            data.msg_type = 1;

            data.id = getpid();

            strcpy(data.msg_text, buffer_msg);

            if(msgsnd(msg_id, (void*) &data, 500, 0) == -1) puts("Message not sent");
        }

        else if(strstr(buffer_msg, "end")){
            data.id = getpid();
            data.msg_type = 1;

            strcpy(data.msg_text, "end");
            msgsnd(msg_id, (void*) &data, 500, 0);

            exit(EXIT_FAILURE);
        }

        else{
            data.id = getpid();
            data.msg_type = 1;
            
            strcpy(data.msg_text, "UNKNOWN COMMAND");
            msgsnd(msg_id, (void*) &data, 500, 0);
        }

    }




    return 0;
}

/**
 * Referensi:
 * https://dextutor.com/program-for-ipc-using-message-queues/
*/